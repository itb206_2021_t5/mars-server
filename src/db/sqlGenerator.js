const CONST = require("../consts/statement");

const sqlGenerator = (name) => {
  switch (name) {
    case CONST.GET_PRODUCT_BY_CODE:
      return `
        select
          distinct p.product_id as "pid",
          p.product_barcode as "barcode",
          ps.size_name,
          pd.product_name,
          pb.brand_name,
          ps.size_name,
          pp.price
        from
          allproducts p,
          product_details pd,
          product_brand pb,
          product_price pp,
          product_size ps,
          inventory iv
        where
          p.product_barcode = :barcode
          and iv.shop_id = :shopid
          and p.product_common_id = pd.product_common_id
          and p.size_id = ps.size_id
          and pd.brand_id = pb.brand_id
          and pp.product_common_id = p.product_common_id
          and iv.product_id = p.product_id
        `;
    case CONST.GET_PRODUCT_DETAIL_BY_CODE:
      return `
        select
          distinct p.product_id as "pid",
          p.product_barcode as "barcode",
          p.product_common_id as "pcid",
          iv.qty,
          pd.product_name,
          pd.product_images,
          pb.brand_name,
          pd.product_desc,
          pc.category_name,
          psc.subcategory_name,
          ps.size_name,
          pp.retail_price,
          pp.price
        from
          allproducts p,
          product_details pd,
          product_brand pb,
          product_price pp,
          product_size ps,
          product_subcategory psc,
          product_category pc,
          inventory iv
        where
          p.product_barcode = :barcode
          and iv.shop_id = :shopid
          and p.product_common_id = pd.product_common_id
          and p.size_id = ps.size_id
          and pd.brand_id = pb.brand_id
          and pp.product_common_id = p.product_common_id
          and iv.product_id = p.product_id
          and pc.category_id = pd.category_id
          and psc.subcategory_id = pd.subcategory_id
        `;
    case CONST.GET_ALL_PRODUCTS_SIZE_BY_CODE:
      return `
        select
          ps.size_name,
          iv.qty,
          p.product_id as "pid"
        from
          allproducts p,
          product_details pd,
          product_brand pb,
          product_price pp,
          product_size ps,
          inventory iv 
        where
          pd.product_common_id = :commonid
          and p.product_common_id = pd.product_common_id
          and pd.brand_id = pb.brand_id
          and p.size_id = ps.size_id
          and pp.product_common_id = p.product_common_id
          and iv.product_id = p.product_id and iv.qty >= '0'
          and iv.shop_id = 'st001'
    `;
    case CONST.GET_ALL_SHOPS_STOCK_BY_PRODUCT_ID:
      return `
        select *
        from 
            inventory iv,
            shop
        where
            iv.product_id = :pid
            and iv.shop_id = shop.shop_id
      `;

    case CONST.GET_ALL_PRODUCTS:
      return `
        select 
          distinct p.product_id as "pid",
          p.product_barcode,
          pd.product_name,
          pb.brand_name,
          ps.size_name,
          pp.price
        from 
          allproducts p,
          inventory iv,
          product_details pd,
          product_brand pb,
          product_price pp,
          product_size ps,
          shop
        where
          iv.shop_id = :shopid
          and p.product_common_id =pd.product_common_id
          and p.size_id = ps.size_id
          and pd.brand_id = pb.brand_id
          and iv.qty > 0
          and iv.shop_id = shop.shop_id
      `;
    case CONST.GET_ALL_PAYMENTS:
      return `
        SELECT
          p.payment_id,
          p.payment_code,
          p.payment_name,
          p.payment_type
        FROM
          payment        p,
          payment_branch pb
        WHERE
          pb.shop_id = :shopid
          AND pb.active = 'Y'
          AND p.payment_id = pb.payment_id
      `
    case CONST.GET_MEMBER_BY_BODE:
      return `
        SELECT 
          *
        from
          member
        WHERE
          member_code = :code
      `

    case CONST.INSERT_TRANSACTION:
      return `
      INSERT INTO
        transaction (
          transaction_id,
          member_id,
          shop_id,
          payment_id,
          device_id,
          created_at,
          updated_at,
          amount,
          transaction_no
      )
      VALUES (
        :tid,
        :memberid,
        :shopid,
        :paymentid,
        :deviceid,
        to_timestamp(:datetime,'yyyy-mm-dd hh24:mi:ss'),
        to_timestamp(:datetime,'yyyy-mm-dd hh24:mi:ss'),
        :amount,
        auto_increment.nextval
      )
      `

      case CONST.INSERT_TRANSACTION_DETAIL:
        return `
          INSERT INTO
            transaction_detail (
              transaction_id,
              product_id,
              qty,
              product_listed_name,
              product_listed_price
          )
          VALUES (
            :tid,
            :pid,
            :qty,
            :name,
            :price
          )
        `

      case CONST.UDPATE_MEMBER_POINTS:
        return `
          UPDATE
            member
          SET
            points = (
              SELECT
                points + :inc
              FROM
                member
              WHERE
                member_id = :mid
            )
          WHERE
            member_id = :mid
        `
      case CONST.UPDATE_INVENTORY:
        return `
          UPDATE
            inventory
          SET
            qty = (
              SELECT
                qty - :dec
              FROM
                inventory
              WHERE
                shop_id = :shopid
                and product_id = :pid
            )
          WHERE
            shop_id = :shopid
            and product_id = :pid
        `
      case CONST.GET_TRANSACTION_NO_BY_ID:
        return `
          SELECT
              transaction_no
          FROM
              transaction
          WHERE
              transaction_id = :tid
        `
  }
};

module.exports = sqlGenerator;
