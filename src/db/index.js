const oracledb = require("oracledb");
const config = require("../config/db.config");

async function sql(statement, params, options, isExecuteMany = false) {
  let connection;
  try {
    connection = await oracledb.getConnection({
      user: config.USER,
      password: config.PASSWORD,
      connectString: config.HOST,
    });
    if (isExecuteMany) {
      return connection.executeMany(statement, params || [], {
        autoCommit: true,
        ...options,
      });
    }
    return connection.execute(statement, params || [], { autoCommit: true });
  } catch (err) {
    console.error(err.message);
    throw error;
  } finally {
    if (connection) {
      try {
        // Always close connections
        await connection.close();
        console.log("close connection success");
      } catch (err) {
        console.error(err.message);
      }
    }
  }

  //   return connection;
}

module.exports = sql;
