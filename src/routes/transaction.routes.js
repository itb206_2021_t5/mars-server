const transaction = require("../controllers/transaction.controller");

module.exports = (app) => {
  const router = require("express").Router();
  router.post("/", transaction.create);

  app.use("/api/transaction", router);
};
