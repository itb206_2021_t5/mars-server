const payment = require("../controllers/payment.controller");

module.exports = (app) => {
  const router = require("express").Router();
  router.get("/", payment.getAllPayments);

  app.use("/api/payment", router);
};
