const stock = require('../controllers/stock.controller')

module.exports = (app) => {
  const router = require("express").Router();
  router.get("/:id", stock.getStockById);

  app.use("/api/stock", router);
};