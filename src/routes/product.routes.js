const product = require("../controllers/product.controller");

module.exports = (app) => {
  const router = require("express").Router();
  router.get("/", product.getAllProducts);
  router.get("/:code", product.getProductByCode);
  router.get("/detail/:code", product.getProductDetailByCode);

  app.use("/api/product", router);
};
