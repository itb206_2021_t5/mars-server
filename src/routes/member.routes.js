const member = require("../controllers/member.controller");

module.exports = (app) => {
  const router = require("express").Router();
  router.get("/:code", member.getMemberByCode);

  app.use("/api/member", router);
};
