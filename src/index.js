require('dotenv').config()

const express = require('express')
const db = require('./db')
const cors = require('cors')
const app = express()

var corsOptions = {
  origin: "http://localhost:8080"
};

app.use(cors(corsOptions));


app.use(express.json())
app.use(express.urlencoded({ extended: true}))


app.get('/', (req, res) => {
  res.send('hello world')
})

require("./routes/product.routes.js")(app);
require("./routes/stock.routes.js")(app);
require("./routes/payment.routes.js")(app);
require("./routes/member.routes.js")(app);
require("./routes/transaction.routes.js")(app);

// set port, listen for requests
const port = process.env.PORT ||4000
app.listen(port, () => console.log("MarathonSport Server is listending on port %s!", port))