const sql = require("../db");
const sqlGenerator = require("../db/sqlGenerator");
const helper = require("../helper");
const CONST = require("../consts/statement");
const uuid = require("uuid");
const OracleDB = require("oracledb");

// constructor
const Transaction = function (t) {
  this.shop_id = t.shop_id;
  this.member_id = t.member_id;
  this.payment_id = t.payment_id;
  this.device_id = t.device_id;
  this.products = t.products;
  this.tid = uuid.v4();
};

Transaction.create = async (transaction, cb) => {
  /** FOUR major steps
   * 1) insert data to `TRANSACTION`
   * 2) insert data to `TRANSACTION_DETAIL`
   * 3) update `INVENTORY` qty for specified product + shop
   * 4) update `MEMBER` POINTS
   * 5) return order number to front-end to inform transaction done
   */

  // todo: some improvement todo, price should get from db, instead of from client uplaod
  // calculate total amount from product list
  const amount = transaction.products.reduce((acc, obj) => {
    return acc + obj.qty * obj.price;
  }, 0);

  const completedTask = [];
  let result;
  let result2;
  let result3;
  let result4;

  try {
    // 1) insert data to `TRANSACTION`
    result = await sql(sqlGenerator(CONST.INSERT_TRANSACTION), {
      tid: transaction.tid,
      memberid: transaction.member_id || "",
      paymentid: transaction.payment_id,
      deviceid: transaction.device_id,
      shopid: transaction.shop_id,
      datetime: "2021-11-11 18:23:23",
      amount,
    });

    if (result.rowsAffected < 1) {
      cb({ error: "something wrong" }, null);
      return;
    }

    // // inject transaction id to each product
    const detailData = transaction.products.map((p) => ({
      ...p,
      tid: transaction.tid,
    }));
    // console.log(detailData);

    // 2) insert product data to `TRANSACTION_DETAIL`
    result2 = await sql(
      sqlGenerator(CONST.INSERT_TRANSACTION_DETAIL),
      detailData,
      {
        bindDefs: {
          qty: { type: OracleDB.NUMBER },
          price: { type: OracleDB.NUMBER },
          pid: { type: OracleDB.STRING, maxSize: 40 },
          name: { type: OracleDB.STRING, maxSize: 40 },
          tid: { type: OracleDB.STRING, maxSize: 38 },
        },
      },
      true
    );
    // console.log(result2.rowsAffected);

    // 3) update member point
    // incremented is the total amount but in integer
    if (transaction.member_id) {
      const inc = Math.trunc(amount);
      result3 = await sql(sqlGenerator(CONST.UDPATE_MEMBER_POINTS), {
        mid: transaction.member_id,
        inc,
      });
      console.log(result3.rowsAffected);
    }

    // 4) update inventory
    const inventoryData = transaction.products.map((p) => ({
      dec: p.qty,
      shopid: transaction.shop_id,
      pid: p.pid,
    }));

    result4 = await sql(
      sqlGenerator(CONST.UPDATE_INVENTORY),
      inventoryData,
      {
        bindDefs: {
          dec: { type: OracleDB.NUMBER },
          pid: { type: OracleDB.STRING, maxSize: 10 },
          shopid: { type: OracleDB.STRING, maxSize: 10 },
        },
      },
      true
    );

    if (result && result.rowsAffected) {
      completedTask.push(
        `[CREATED] transaction ${result.rowsAffected} records affected`
      );
    }
    if (result2 && result2.rowsAffected) {
      completedTask.push(
        `[CREATED] transaction detail ${result2.rowsAffected} records affected`
      );
    }
    if (result3 && result3.rowsAffected) {
      completedTask.push(
        `[UPDATED] member ${result.rowsAffected} records affected`
      );
    }
    if (result4 && result4.rowsAffected) {
      completedTask.push(
        `[UPDATED] inventory ${result4.rowsAffected} records affected`
      );
    }

    const orderResult = await sql(
      sqlGenerator(CONST.GET_TRANSACTION_NO_BY_ID),
      { tid: transaction.tid }
    );

    cb(null, { log: completedTask, transaction_no: helper.transformObject(orderResult).transaction_no });
  } catch (err) {
    console.log(err);
    cb(err, null);
  }
};

module.exports = Transaction;
