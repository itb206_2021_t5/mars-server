const sql = require("../db");
const sqlGenerator = require("../db/sqlGenerator");
const helper = require('../helper')
const CONST = require("../consts/statement");

const shopid = "st001";

// get single product by barcode (shopid given)
exports.getProductByCode = async (req, res) => {
    const barcode = req.params.code || "20840221";
    try {
        let product
    
        // get single product with detail
        const singleResult = await sql(sqlGenerator(CONST.GET_PRODUCT_BY_CODE), {
          shopid,
          barcode,
        });
    
        product = helper.transformObject(singleResult)
    
        if (!product) {
            res.status(404).json({ msg: 'product not found'})
        }

        res.json({
          response: product,
        });
      } catch (err) {
        console.log(err);
        res.status(500).json({ error: 'some problems'})
      }
}


// get single product with size options by barcode (shopid given)
exports.getProductDetailByCode = async (req, res) => {
  const barcode = req.params.code || "20840221";

  try {
    let product

    // get single product with detail
    const singleResult = await sql(sqlGenerator(CONST.GET_PRODUCT_DETAIL_BY_CODE), {
      shopid,
      barcode,
    });

    product = helper.transformObject(singleResult)
    if (!product) {
        res.status(404).json({ msg: 'product not found'})
        return
    }
    // if product exist, fetch size options for this product
    const sizeResult = await sql(
      sqlGenerator(CONST.GET_ALL_PRODUCTS_SIZE_BY_CODE),
      {
        commonid: product.pcid,
      }
    );

    product.options = helper.transformList(sizeResult);

    res.json({
      response: product,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: 'some problems'})
  }
};


// get all products (shopid given)
exports.getAllProducts = async (req, res) => {
    try {
        const result = await sql(sqlGenerator(CONST.GET_ALL_PRODUCTS), {
            shopid
        })
        res.json({
            response: helper.transformList(result)
        })
    } catch (err) {
        console.log(err);
        res.status(500).json({ error: 'some problems'})
    }
}
