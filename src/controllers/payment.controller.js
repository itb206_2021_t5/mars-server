const sql = require("../db");
const sqlGenerator = require("../db/sqlGenerator");
const helper = require('../helper')
const CONST = require("../consts/statement");

const shopid = "st001";

// get all payments for the shop (shopid given)
exports.getAllPayments = async (req, res) => {
    try {
        // get single product with detail
        const result = await sql(sqlGenerator(CONST.GET_ALL_PAYMENTS), {
          shopid,
        });
    
        const payments = helper.transformList(result)
    
        if (!payments) {
            res.status(404).json({ msg: 'not found'})
        }

        res.json({
          response: payments,
        });
      } catch (err) {
        console.log(err);
        res.status(500).json({ error: 'some problems'})
      }
}
