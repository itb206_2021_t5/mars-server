const sql = require("../db");
const sqlGenerator = require("../db/sqlGenerator");
const helper = require("../helper");
const CONST = require("../consts/statement");
const Transaction = require("../models/transaction.model");
const shopid = "st001";
const deviceid = "stdec001";

/** POST body should have 
#1 sql - insert transaction table
shop_id
member_id (nullable)
payment_id
device_id
transaction_id (server gen)
created_at (server gen)
updated_at (server gen)

#2 sql - insert transaction_detail table
product_id
qty
product_name
product_price

amount (system calculation)





*/
const sampleProducts = [
//   {
//     pid: "adis002",
//     qty: 1,
//     price: 398,
//     name: "TShirt 1",
//   },
//   {
//     pid: "adim002",
//     qty: 1,
//     price: 318.3,
//     name: "TShirt 2",
//   },
  // {
  //   pid: "adil002",
  //   qty: 1,
  //   price: 398,
  //   name: "TShirt 1",
  // },
  // {
  //   pid: "adixl002",
  //   qty: 3,
  //   price: 318.3,
  //   name: "TShirt 2",
  // },
  // {
  //   pid: "adil002",
  //   qty: 1,
  //   price: 398,
  //   name: "TShirt 1",
  // },
  // {
  //   pid: "adixl002",
  //   qty: 3,
  //   price: 318.3,
  //   name: "TShirt 2",
  // },
];

// get member by code (shopid given)
exports.create = async (req, res) => {
  if (!req.body) {
    res.status(400).json({ error: "something wrong" });
  }

  const data = req.body;
  // create a transaction
  const transaction = new Transaction({
    shop_id: shopid,
    member_id: data.mid,
    payment_id: data.pyid,
    device_id: deviceid,
    products: data.products,
  });

  // save to db
  Transaction.create(transaction, (err, data) => {
    if (err) {
      res.status(500).json({ error: "something wrong" });
      return;
    }
    res.json({
      response: data,
    });
  });
};
