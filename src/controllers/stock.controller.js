const sql = require("../db");
const sqlGenerator = require("../db/sqlGenerator");
const CONST = require("../consts/statement");
const helper = require('../helper')

const shopid = "st001";

exports.getStockById = async (req, res) => {
    const pid = req.params.id
  try {
    const result = await sql(sqlGenerator(CONST.GET_ALL_SHOPS_STOCK_BY_PRODUCT_ID), {
        pid,
    });
    res.json({
        response: helper.transformList(result),
    })
  } catch (err) {}
};
