const sql = require("../db");
const sqlGenerator = require("../db/sqlGenerator");
const helper = require("../helper");
const CONST = require("../consts/statement");

// get member by code (shopid given)
exports.getMemberByCode = async (req, res) => {
  const code = req.params.code;

  try {
    const result = await sql(sqlGenerator(CONST.GET_MEMBER_BY_BODE), {
      code,
    });

    const member = helper.transformObject(result);

    if (!member) {
      res.status(404).json({ msg: "not found" });
    }

    res.json({
      response: member,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "some problems" });
  }
};
