const transformList = (result) => {
  if (!result.rows.length) {
    return [];
  }
  const list = [];
  const field = result.metaData.map((f) => f.name.toLowerCase());

  result.rows.forEach((r) => {
    const obj = {};
    r.forEach((i, index) => {
      obj[field[index]] = i;
    });
    list.push(obj);
  });

  return list;
};

const transformObject = (result) => {
  if (!result.rows.length) {
    return null;
  }
  const obj = {};

  const field = result.metaData.map((f) => f.name.toLowerCase());

  result.rows.forEach((r) => {
    r.forEach((i, index) => {
      obj[field[index]] = i;
    });
  });

  return obj;
};

module.exports = {
  transformList,
  transformObject
};
