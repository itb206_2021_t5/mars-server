FROM oraclelinux:7-slim

RUN  yum -y install oracle-release-el7 oracle-nodejs-release-el7 && \
     yum-config-manager --disable ol7_developer_EPEL && \
     yum -y install oracle-instantclient19.3-basiclite nodejs && \
     rm -rf /var/cache/yum

ADD src/ /src
WORKDIR /src


# WORKDIR /myapp
# ADD package.json /myapp/
# ADD index.js /myapp/
RUN npm install
CMD ["npm", "start"]
# CMD exec node index.js